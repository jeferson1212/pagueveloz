# Cadastro de Empresas - PagueVeloz

Este projeto é uma avaliação referente a vaga de Desenvolvedor C# para a empresa PagueVeloz.

## Tecnologias e Gerenciador de Banco de Dados Utilizados:

- [.NET Core](https://dotnet.microsoft.com/)
- [Angular](https://angular.io)
- [Bootstrap](https://getbootstrap.com)
- [EntityFramework Core](https://github.com/dotnet/efcore)
- [Microsoft SQL Server](https://www.microsoft.com/pt-br/sql-server/)

---

## Como Instalar?

Siga os seguintes passos para instalar e testar a aplicação:

1. Clonar a aplicação.
2. Rodar o comando **dotnet restore build**.
3. Executar o script SQL no SQL Server para criar as tabelas.
4. Executar o script de Insert para inserir os estados.
5. Configuar o arquivo **app.config.js** para a base de testes da avaliação
6. Rodar o comando no PowerShell **dotnet run**.
7. Testar no Postman

## ATENÇÃO

Infelizmente por problemas técnicos, envio **somente** backend .NET Core mas ele seria feito em Angular
com Bootstrap.