using AutoMapper;
using pagueveloz.Models;
using pagueveloz.ViewModel;

namespace pagueveloz.Mapper
{
    public class MappingProfilesViewModel: Profile
    {
        public MappingProfilesViewModel()
        {
            CreateMap<Empresa, EmpresaViewModel>()
                .ForMember(x => x.EstadoNome, opt => opt.MapFrom(y => y.Estado.Nome))
                .ForMember(x => x.EstadoSigla, opt => opt.MapFrom(y => y.Estado.Sigla));

            CreateMap<Fornecedor, FornecedorViewModel>()
                .ForMember(x => x.EmpresaNome, opt => opt.MapFrom(y => y.Empresa.NomeFantasia))
                .ForMember(x => x.DataNascimento, opt => opt.MapFrom(y => y.DataNacimento.ToString("dd/MM/yyyy")));

            CreateMap<Estado, EstadoViewModel>();
        }
    }
}