using System;
using AutoMapper;
using pagueveloz.Models;
using pagueveloz.ViewModel;

namespace pagueveloz.Mapper
{
    public class MappingProfilesModel: Profile
    {
        public MappingProfilesModel()
        {
            CreateMap<EmpresaViewModel, Empresa>();
            CreateMap<FornecedorViewModel, Fornecedor>()
                .ForMember(x => x.DataNacimento, opt => opt.MapFrom(y => Convert.ToDateTime(y.DataNascimento)));
            CreateMap<EstadoViewModel, Estado>();
        }
    }
}