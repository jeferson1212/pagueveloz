namespace pagueveloz.ViewModel
{
    public class EmpresaViewModel
    {
        public int Id { get; set; }
        public string Cnpj { get; set; }
        public string NomeFantasia { get; set; }
        public int EstadoId { get; set; }
        public string EstadoNome { get; set; }
        public string EstadoSigla { get; set; }

    }
}