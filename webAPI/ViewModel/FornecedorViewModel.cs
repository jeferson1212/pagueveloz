using System;

namespace pagueveloz.ViewModel
{
    public class FornecedorViewModel
    {
        public int Id { get; set; }
        public string Nome { get; set; }
        public string Cpf { get; set; }
        public string Cnpj { get; set; }
        public string Rg { get; set; }
        public long Telefone1 { get; set; }
        public long? Telefone2 { get; set; }
        public long? Telefone3 { get; set; }
        public int EmpresaId { get; set; }
        public string EmpresaNome { get; set; }
        public string DataNascimento { get; set; }
    }
}