namespace pagueveloz.ViewModel
{
    public class EstadoViewModel
    {
        public int Id { get; set; }
        public string Nome { get; set; }
        public string Sigla { get; set; }
    }
}