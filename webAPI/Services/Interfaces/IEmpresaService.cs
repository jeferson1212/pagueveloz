using System.Collections.Generic;
using pagueveloz.ViewModel;

namespace pagueveloz.Services.Interfaces
{
    public interface IEmpresaService
    {
        IEnumerable<EmpresaViewModel> findAll();
        IEnumerable<EmpresaViewModel> findByFilter(EmpresaViewModel viewModel);
        EmpresaViewModel findById(int id);
        EmpresaViewModel findByCnpj(string cnpj);
        void Create(EmpresaViewModel viewModel);
        void Update(EmpresaViewModel viewModel);
        void Delete(int id);
    }
}