using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using pagueveloz.ViewModel;

namespace pagueveloz.Services.Interfaces
{
    public interface IFornecedorService
    {
        IEnumerable<FornecedorViewModel> findAll();
        void Create(FornecedorViewModel entity);
        void Update(FornecedorViewModel entity);
        void Delete(int id);
        FornecedorViewModel findById(int id);
    }   
}