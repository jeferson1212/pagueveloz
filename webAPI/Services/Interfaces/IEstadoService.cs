using System.Collections.Generic;
using pagueveloz.ViewModel;

namespace pagueveloz.Services.Interfaces
{
    public interface IEstadoService
    {
        IEnumerable<EstadoViewModel> findAll();
    }
}