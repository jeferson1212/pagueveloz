using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using AutoMapper;
using pagueveloz.Models;
using pagueveloz.Repository.Interface;
using pagueveloz.Services.Interfaces;
using pagueveloz.ViewModel;

namespace pagueveloz.Services.Implements
{
    public class EstadoService : IEstadoService
    {
        private readonly IEstadoRepository _Repository;
        private readonly IMapper _Mapper;
        public EstadoService(IEstadoRepository Repository,
                             IMapper Mapper)
        {
            _Repository = Repository;
            _Mapper = Mapper;
        }

        public IEnumerable<EstadoViewModel> findAll()
        {
            var listEnumerable = _Repository.findAll().Select(x => _Mapper.Map<EstadoViewModel>(x)).AsEnumerable();
            return listEnumerable;;
        }
    }
}