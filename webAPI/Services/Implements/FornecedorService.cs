using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using AutoMapper;
using pagueveloz.Models;
using pagueveloz.Repository.Interface;
using pagueveloz.Services.Interfaces;
using pagueveloz.ViewModel;

namespace pagueveloz.Services.Implements
{
    public class FornecedorService : IFornecedorService
    {
        private readonly IFornecedorRepository _FornecedorRepository;
        private readonly IEmpresaRepository _EmpresaRepository;
        private readonly IMapper _Mapper;
        public FornecedorService(IFornecedorRepository FornecedorRepository,
                                 IEmpresaRepository EmpresaRepository,
                                 IMapper Mapper)
        {
            _FornecedorRepository = FornecedorRepository;
            _EmpresaRepository = EmpresaRepository;
            _Mapper = Mapper;

        }public void Create(FornecedorViewModel entity)
        {
            if(!String.IsNullOrEmpty(entity.Cpf))
            {
                ValidaPessoaFisica(entity);
            }
            _FornecedorRepository.Create(_Mapper.Map<Fornecedor>(entity));
            _FornecedorRepository.SaveChanges();
        }

        public void Delete(int id)
        {
            var model = _FornecedorRepository.findByPredicate(x => x.Id == id).SingleOrDefault();
            _FornecedorRepository.Delete(model);
            _FornecedorRepository.SaveChanges();
        }

        public IEnumerable<FornecedorViewModel> findAll()
        {
            var listEnumerable = _FornecedorRepository.findAll().Select(x => _Mapper.Map<FornecedorViewModel>(x)).AsEnumerable();
            return listEnumerable;
        }

        public IEnumerable<FornecedorViewModel> findByFilter(FornecedorViewModel viewModel)
        {
            var listEnumerable = _FornecedorRepository.findByPredicate(x => x.Id == viewModel.Id).Select(x => _Mapper.Map<FornecedorViewModel>(x)).AsEnumerable();
            return listEnumerable;
        }

        public FornecedorViewModel findById(int id)
        {
            return _Mapper.Map<FornecedorViewModel>(_FornecedorRepository.findByPredicate(x => x.Id == id, y => y.Empresa).SingleOrDefault());
        }

        public void Update(FornecedorViewModel entity)
        {
            if(!String.IsNullOrEmpty(entity.Cpf))
            {
                ValidaPessoaFisica(entity);
            }
            _FornecedorRepository.Update(_Mapper.Map<Fornecedor>(entity));
            _FornecedorRepository.SaveChanges();
        }

        private void ValidaPessoaFisica(FornecedorViewModel model)
        {
            string messageError = string.Empty;
            var birthdate = Convert.ToDateTime(model.DataNascimento);
            if(string.IsNullOrEmpty(model.Rg))
            {
                messageError += "Campo RG é obrigatório!" + Environment.NewLine;
            }
            if(birthdate == DateTime.MinValue) 
            {
                messageError += "Campo Data de Nascimento é Obrigatório!" + Environment.NewLine;
            }
            var empresa = _EmpresaRepository.findByPredicate(x => x.Id == model.EmpresaId, y => y.Estado)
                .SingleOrDefault();
            if(empresa.Estado.Sigla.Equals("PR"))
            {
                var today = DateTime.Now;
                var age = today.Year - birthdate.Year;
                if(birthdate > today.AddYears(-age))
                {
                    age--;
                }

                if(age < 18) 
                {
                    messageError += "Fornecedor deste estado precisa ser maior de 18 anos.";
                }
            }
            if(!string.IsNullOrEmpty(messageError))
            {
                throw new Exception(messageError);
            }
        }
    }
}