using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using AutoMapper;
using pagueveloz.Models;
using pagueveloz.Repository.Interface;
using pagueveloz.Services.Interfaces;
using pagueveloz.ViewModel;

namespace pagueveloz.Services.Implements
{
    public class EmpresaService : IEmpresaService
    {
        private readonly IEmpresaRepository _Repository;
        private readonly IMapper _Mapper;
        public EmpresaService(IEmpresaRepository Repository,
                              IMapper Mapper)
        {
            _Repository = Repository;
            _Mapper = Mapper;
        }
        public void Create(EmpresaViewModel entity)
        {
            _Repository.Create(_Mapper.Map<Empresa>(entity));
            _Repository.SaveChanges();
        }

        public void Delete(int id)
        {
            var model = _Repository.findByPredicate(x => x.Id == id).SingleOrDefault();
            _Repository.Delete(model);
            _Repository.SaveChanges();
        }

        public IEnumerable<EmpresaViewModel> findAll()
        {
            var listEnumerable = _Repository.findAll(f => f.Estado).Select(x => _Mapper.Map<EmpresaViewModel>(x)).AsEnumerable();
            return listEnumerable;
        }

        public EmpresaViewModel findByCnpj(string cnpj)
        {
            return _Mapper.Map<EmpresaViewModel>(
                _Repository.findByPredicate(x => x.Cnpj == cnpj,
                y => y.Estado));
        }

        public IEnumerable<EmpresaViewModel> findByFilter(EmpresaViewModel viewModel)
        {
            var listEnumerable = _Repository.findByPredicate(x => x.Id == viewModel.Id,
                y => y.Estado).Select(x => _Mapper.Map<EmpresaViewModel>(x)).AsEnumerable();
            return listEnumerable;
        }

        public EmpresaViewModel findById(int id)
        {
            var model = _Repository.findByPredicate(x => x.Id == id, y => y.Estado).SingleOrDefault();
            return _Mapper.Map<EmpresaViewModel>(model);
        }

        public void Update(EmpresaViewModel entity)
        {
            _Repository.Update(_Mapper.Map<Empresa>(entity));
            _Repository.SaveChanges();
        }
    }
}