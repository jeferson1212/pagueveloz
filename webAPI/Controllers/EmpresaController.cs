using System;
using System.Collections.Generic;
using System.Net;
using System.Net.Http;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using pagueveloz.Services.Interfaces;
using pagueveloz.ViewModel;

namespace pagueveloz.Controllers
{
    [Route("api/v1/[controller]")]
    public class EmpresaController : Controller
    {
        private readonly IEmpresaService _EmpresaService;

        public EmpresaController(IEmpresaService EmpresaService)
        {
            _EmpresaService = EmpresaService;
        }

        [HttpGet]
        public IEnumerable<EmpresaViewModel> GetAll()
        {
            return _EmpresaService.findAll();
        }

        [HttpGet("{id}")]
        public EmpresaViewModel GetById(int id)
        {
            return _EmpresaService.findById(id);
        }

        [HttpGet("{cnpj}")]
        public EmpresaViewModel GetByCnpj(string cnpj)
        {
            return _EmpresaService.findByCnpj(cnpj);
        }

        [HttpPost]
        public Task<HttpResponseMessage> Save([FromBody] EmpresaViewModel viewModel)
        {
            try
            {
                if(viewModel.Id != 0)
                {
                    _EmpresaService.Update(viewModel);
                } 
                else 
                {
                    _EmpresaService.Create(viewModel);
                }
                return Task.FromResult<HttpResponseMessage>(new HttpResponseMessage(HttpStatusCode.OK));
            } catch
            {
                return Task.FromResult<HttpResponseMessage>(new HttpResponseMessage(HttpStatusCode.InternalServerError));
            }
        }

        [HttpDelete("{id}")]
        public Task<HttpResponseMessage> Delete(int id)
        {
            try
            {
                _EmpresaService.Delete(id);
                return Task.FromResult<HttpResponseMessage>(new HttpResponseMessage(HttpStatusCode.OK));
            } catch (Exception e)
            {
                HttpResponseMessage message = new HttpResponseMessage(HttpStatusCode.InternalServerError);
                message.Content = new StringContent(e.Message);
                return Task.FromResult<HttpResponseMessage>(message);
            }
        }
    }
}