using System;
using System.Collections.Generic;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using pagueveloz.Services.Interfaces;
using pagueveloz.ViewModel;

namespace pagueveloz.Controllers
{
    [Route("api/v1/[controller]")]
    public class FornecedorController : Controller
    {
        private readonly IFornecedorService _FornecedorService;

        public FornecedorController(IFornecedorService FornecedorService)
        {
            _FornecedorService = FornecedorService;
        }

        [HttpGet]
        public IEnumerable<FornecedorViewModel> GetAll()
        {
            return _FornecedorService.findAll();
        }

        [HttpGet("{id}")]
        public FornecedorViewModel GetById(int id)
        {
            return _FornecedorService.findById(id);
        }

        [HttpPost]
        [Consumes("application/json")]
        [Produces("application/json")]
        public Task<HttpResponseMessage> Save([FromBody] FornecedorViewModel viewModel)
        {
            try
            {
                if(viewModel.Id != 0)
                {
                    _FornecedorService.Update(viewModel);
                } 
                else 
                {
                    _FornecedorService.Create(viewModel);
                }
                return Task.FromResult<HttpResponseMessage>(new HttpResponseMessage(HttpStatusCode.OK));
            } catch (Exception e)
            {
                HttpResponseMessage message = new HttpResponseMessage(HttpStatusCode.InternalServerError);
                message.Content = new StringContent(e.Message);
                return Task.FromResult<HttpResponseMessage>(message);
            }
        }

        [HttpDelete("{id}")]
        public Task<HttpResponseMessage> Delete(int id)
        {
            try
            {
                _FornecedorService.Delete(id);
                return Task.FromResult<HttpResponseMessage>(new HttpResponseMessage(HttpStatusCode.OK));
            } catch
            {
                return Task.FromResult<HttpResponseMessage>(new HttpResponseMessage(HttpStatusCode.InternalServerError));
            }
        }
    }
}