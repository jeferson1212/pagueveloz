using System.Collections.Generic;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using pagueveloz.Services.Interfaces;
using pagueveloz.ViewModel;

namespace pagueveloz.Controllers
{
    [Route("api/v1/[controller]")]
    public class EstadoController : Controller
    {
        private readonly IEstadoService _EstadoService;

        public EstadoController(IEstadoService EstadoService)
        {
            _EstadoService = EstadoService;
        }

        [HttpGet]
        public Task<IEnumerable<EstadoViewModel>> GetAll()
        {
            try
            {
                return Task.FromResult<IEnumerable<EstadoViewModel>>(_EstadoService.findAll());
            }
            catch (System.Exception)
            {
                
                throw;
            }
        }
    }
}