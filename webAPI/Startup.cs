using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.HttpsPolicy;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;
using pagueveloz.Mapper;
using pagueveloz.Models;
using pagueveloz.Repository.Implements;
using pagueveloz.Repository.Interface;
using pagueveloz.Services.Implements;
using pagueveloz.Services.Interfaces;

namespace webAPI
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddControllers();
            services.AddDbContext<PagueVelozContext>(options =>
                options.UseSqlServer(Configuration.GetConnectionString("PagueVelozContext")));
            
            //Inject Repository
            services.AddScoped<IEmpresaRepository, EmpresaRepository>();
            services.AddScoped<IEstadoRepository, EstadoRepository>();
            services.AddScoped<IFornecedorRepository, FornecedorRepository>();

            //Inject AutoMapper
            services.AddAutoMapper(typeof(MappingProfilesModel), typeof(MappingProfilesViewModel));

            //Inject Services
            services.AddScoped<IEmpresaService, EmpresaService>();
            services.AddScoped<IEstadoService, EstadoService>();
            services.AddScoped<IFornecedorService, FornecedorService>();
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }

            app.UseHttpsRedirection();

            app.UseRouting();

            app.UseAuthorization();

            app.UseEndpoints(endpoints =>
            {
                endpoints.MapControllers();
            });
        }
    }
}
