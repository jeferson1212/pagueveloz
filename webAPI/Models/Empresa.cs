﻿using System;
using System.Collections.Generic;

namespace pagueveloz.Models
{
    public partial class Empresa
    {
        public Empresa()
        {
            Fornecedor = new HashSet<Fornecedor>();
        }

        public int Id { get; set; }
        public string NomeFantasia { get; set; }
        public string Cnpj { get; set; }
        public int EstadoId { get; set; }

        public virtual Estado Estado { get; set; }
        public virtual ICollection<Fornecedor> Fornecedor { get; set; }
    }
}
