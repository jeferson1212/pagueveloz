﻿using System;
using System.Collections.Generic;

namespace pagueveloz.Models
{
    public partial class Fornecedor
    {
        public int Id { get; set; }
        public string Nome { get; set; }
        public string Cpf { get; set; }
        public string Cnpj { get; set; }
        public string Rg { get; set; }
        public DateTime DataNacimento { get; set; }
        public DateTime DataCadastro { get; set; }
        public long Telefone1 { get; set; }
        public long? Telefone2 { get; set; }
        public long? Telefone3 { get; set; }
        public int EmpresaId { get; set; }

        public virtual Empresa Empresa { get; set; }
    }
}
