﻿using System;
using System.Collections.Generic;

namespace pagueveloz.Models
{
    public partial class Estado
    {
        public Estado()
        {
            Empresa = new HashSet<Empresa>();
        }

        public int Id { get; set; }
        public string Nome { get; set; }
        public string Sigla { get; set; }

        public virtual ICollection<Empresa> Empresa { get; set; }
    }
}
