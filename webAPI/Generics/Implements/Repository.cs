using System;
using System.Linq;
using System.Linq.Expressions;
using Microsoft.EntityFrameworkCore;
using pagueveloz.Generics.Interface;
using pagueveloz.Models;

namespace pagueveloz.Generics.Implements
{
    public class Repository<T> : IRepository<T>, IDisposable where T : class
    {
        private PagueVelozContext _Context { get; set; }
        private DbSet<T> _DbSet;

        public Repository(PagueVelozContext Context)
        {
            _Context = Context;
            _DbSet = _Context.Set<T>();
        }
        public void Create(T entity)
        {
            _Context.Add(entity);
        }

        public void Delete(T entity)
        {
            _DbSet.Remove(entity);
        }

        public IQueryable<T> findAll(params Expression<Func<T, object>>[] objects)
        {
            return objects.Aggregate(_DbSet.AsQueryable(), (current, includeProperty) => current.Include(includeProperty));
        }

        public IQueryable<T> findByPredicate(Expression<Func<T, bool>> predicate, params Expression<Func<T, object>>[] objects)
        {
            var query = _DbSet.Where(predicate);
            return objects.Aggregate(query, (current, includeProperty) => current.Include(includeProperty));
        }

        public void SaveChanges()
        {
            _Context.SaveChanges();
        }

        public void Update(T entity)
        {
            _Context.Set<T>().Update(entity);
        }

        public void Dispose()
        {
            _Context.Dispose();
            GC.SuppressFinalize(this);
        }
    }
}