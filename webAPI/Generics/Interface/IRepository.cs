using System;
using System.Linq;
using System.Linq.Expressions;

namespace pagueveloz.Generics.Interface
{
    public interface IRepository<T> where T: class
    {
        IQueryable<T> findAll(params Expression<Func<T, object>>[] objects);
        IQueryable<T> findByPredicate(Expression<Func<T, bool>> predicate, params Expression<Func<T, object>>[] objects);
        void Create(T entity);
        void Update(T entity);
        void Delete(T entity);
        void SaveChanges();
    }
}