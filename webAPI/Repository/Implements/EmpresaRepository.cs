using System;
using pagueveloz.Generics.Implements;
using pagueveloz.Models;
using pagueveloz.Repository.Interface;

namespace pagueveloz.Repository.Implements
{
    public class EmpresaRepository : Repository<Empresa>, IEmpresaRepository
    {
        public EmpresaRepository(PagueVelozContext Context) 
            : base(Context)
        {
        }
    }
}