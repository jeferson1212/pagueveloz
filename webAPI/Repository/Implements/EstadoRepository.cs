using pagueveloz.Generics.Implements;
using pagueveloz.Models;
using pagueveloz.Repository.Interface;

namespace pagueveloz.Repository.Implements
{
    public class EstadoRepository : Repository<Estado>, IEstadoRepository
    {
        public EstadoRepository(PagueVelozContext Context) 
            : base(Context)
        {
        }
    }
}