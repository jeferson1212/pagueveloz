using pagueveloz.Generics.Implements;
using pagueveloz.Models;
using pagueveloz.Repository.Interface;

namespace pagueveloz.Repository.Implements
{
    public class FornecedorRepository : Repository<Fornecedor>, IFornecedorRepository
    {
        public FornecedorRepository(PagueVelozContext Context) 
            : base(Context)
        {
        }
    }
}