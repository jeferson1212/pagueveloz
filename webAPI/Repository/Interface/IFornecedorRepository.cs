using pagueveloz.Generics.Interface;
using pagueveloz.Models;

namespace pagueveloz.Repository.Interface
{
    public interface IFornecedorRepository: IRepository<Fornecedor>
    {
        
    }
}