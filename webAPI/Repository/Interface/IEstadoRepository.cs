using pagueveloz.Generics.Interface;
using pagueveloz.Models;

namespace pagueveloz.Repository.Interface
{
    public interface IEstadoRepository: IRepository<Estado>
    {
    }
}